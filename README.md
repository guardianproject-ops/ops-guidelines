# Guardian Project :: Ops :: Guidelines

Best practices we follow for our ops work.

# Ansible 

Ansible is an IT automation tool. It can configure systems, deploy software,
and orchestrate more advanced IT tasks such as continuous deployments or zero
downtime rolling updates. Its similar to Chef, or puppet, or homegrown bash
scripts.. but better than all of them.

## Our Guidelines

### Do not trust Ansible Galaxy

Ansible comes with a pypi/npm style repository of pre-built roles called Ansible
Galaxy. Galaxy is great, there's lots of great stuff in there that will help
you save time and make your life easier.

But using Ansible Galaxy is literally giving a stranger root access to your box.

We *never* depend directly on a role from Galaxy. Instead we first audit the
role at the version we want to use (probably the latest one), then pin it in
`requirements.yml` with the specific SHA1 hash of the commit.

If/when you want to update it, you must review all changes since that SHA1.

### Custom Roles

All new/custom roles should go in their own repository under the
[guardianproject-ops](https://gitlab.com/guardianproject-ops) repository.

Each role should be standalone and fully documented with sane and clearly
defined defaults. Dependencies among roles should be clearly documented.

When writing plays, we *always* use the yaml style and not the `foo=bar` style.

### Idempotency

All playbooks should be idempotent i.e., if run once they should bring the
machine(s) to the desired state, and if run again N times after that they
should make 0 changes (because the machine(s) are in the desired state).
Please make sure your playbooks are idempotent.

### Can be run anytime

When a playbook or change is checked into ansible you should assume that it
could be run at ANY TIME. Always make sure the checked in state is the desired
state. Always test changes when they land so they don't surprise you later. 

### Ansible Resouces

Get going with Ansible

* [The official docs](http://docs.ansible.com/ansible/latest/)
* [Basic tutorial](https://serversforhackers.com/c/an-ansible2-tutorial)
* [Ansible tips and tricks](https://ansible-tips-and-tricks.readthedocs.io/en/latest/ansible/commands/)

